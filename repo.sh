wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | sudo apt-key add -
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1C61A2656FB57B7E4DE0F4C1FC918B335044912E
curl -L https://packagecloud.io/dimkr/vscodium/gpgkey | sudo apt-key add -
echo "deb http://deb.anydesk.com/ all main" | sudo tee /etc/apt/sources.list.d/anydesk.list
echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main"  | sudo tee /etc/apt/sources.list.d/atom.list
echo "deb [arch=i386,amd64] http://linux.dropbox.com/ubuntu cosmic main" | sudo tee /etc/apt/sources.list.d/dropbox.list
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublimetext.list
echo "deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main"  | sudo tee /etc/apt/sources.list.d/vscodium.list
sudo apt -y update
